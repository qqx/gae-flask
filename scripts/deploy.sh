#!/usr/bin/env bash

pip install -r requirements.txt -t lib/

echo $GCLOUD_KEY > key.json

gcloud auth activate-service-account $GCLOUD_ACCOUNT --key-file key.json
gcloud --quiet config set project $GCP_PROJECT
gcloud preview app gen-repo-info-file
gcloud --quiet preview app deploy app.yaml --no-promote --version $GCP_PROJECT_VERSION
