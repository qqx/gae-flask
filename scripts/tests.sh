#!/usr/bin/env bash

virtualenv env
source env/bin/activate

# install test env packages from requirements.testing.txt
pip install -r requirements.testing.txt

# link env as lib folder
pip install git+https://github.com/ze-phyr-us/linkenv.git
linkenv env/lib/python2.7/site-packages lib

# run tests
python runtests.py